<?php

error_reporting(E_ALL);

require __DIR__ . '/../vendor/autoload.php';

$application = new Symfony\Component\Console\Application();
$application->add(new \HomeMe\Morpherd\RunCommand());
$application->run();
