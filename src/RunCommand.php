<?php

namespace HomeMe\Morpherd;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class RunCommand extends Command
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('morphed:run')
            ->setDescription('Run php morpher daemon')
            ->addArgument(
                'port',
                InputArgument::REQUIRED,
                'Port for binding'
            )
            ->addOption(
                'host',
                null,
                InputOption::VALUE_OPTIONAL,
                'Hostname for binding'
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $host = $input->getOption('host') ?: '127.0.0.1';

        $server = new Server($host, $input->getArgument('port'));

        $output->writeln(sprintf('<info>Binding on %s:%d</info>', $server->getHost(), $server->getPort()));

        $server->run();
    }
}
