<?php

namespace HomeMe\Morpherd;

use HomeMe\Morpher\LocalMorpher;

final class Application
{
    /**
     * @var LocalMorpher
     */
    private $morpher;

    /**
     */
    public function __construct()
    {
        $this->morpher = new LocalMorpher();
    }

    /**
     * @param string $route
     * @param array $params
     * @return array
     */
    public function dispatch($route, array $params) {
        switch ($route) {
            case '/morphology/inflect':
                $text = isset($params['text']) ? $params['text'] : '';
                $case = isset($params['case']) ? $params['case'] : '';

                return ['result' => $this->morpher->inflect($text, $case)];
            case '/morphology/gender':
                $text = isset($params['text']) ? $params['text'] : '';

                return ['result' => $this->morpher->gender($text)];
            default:
                throw new \InvalidArgumentException('Unknown route: ' . $route);
        }
    }
}