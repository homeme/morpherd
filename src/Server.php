<?php

namespace HomeMe\Morpherd;

use React\EventLoop\Factory;
use React\Http\Request;
use React\Http\Response;
use React\Http\Server as HttpServer;
use React\Socket\Server as SocketServer;

final class Server
{
    const VERSION = '0.1';

    /**
     * @var string
     */
    private $host;
    /**
     * @var int
     */
    private $port;

    /**
     * @var Application
     */
    private $application;

    /**
     * @param string $host
     * @param int $port
     */
    public function __construct($host, $port)
    {
        $this->host = (string)$host;
        $this->port = (int)$port;
        $this->application = new Application();
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @throws \React\Socket\ConnectionException
     * @return void
     */
    public function run() {
        $loop = Factory::create();
        $socket = new SocketServer($loop);
        $http = new HttpServer($socket);

        $http->on('request', function(Request $request, Response $response) {
            $this->onRequest($request, $response);
        });

        $socket->listen($this->port, $this->host);
        $loop->run();
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return void
     */
    private function onRequest(Request $request, Response $response) {
        $result = [
            'version' => self::VERSION,
            'success' => true,
            'response' => [],
        ];

        try {
            $result['response'] = $this->application->dispatch($request->getPath(), $request->getQuery());
        } catch (\Exception $e) {
            $result['success'] = false;
            $result['response'] = ['errorMessage' => $e->getMessage()];
        }

        $headers = array('Content-Type' => 'application/json');
        $response->writeHead(200, $headers);
        $response->end(json_encode($result, JSON_UNESCAPED_UNICODE));
    }
}